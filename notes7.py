# INHERITANCE
class Animal():

    def __init__(self):
        print("Animal created")

    def whoAmi(self):
        print("Animal")

    def eat(self):
        print("Eating")


class Dog(Animal):

    def __init__(self):
        Animal.__init__(self)
        print("Dog created")

    def bark(self):
        print("Woof")

    def eat(self):
        print("Dog Eating")



mya = Animal()
mya.whoAmi()
mya.eat()
mydog = Dog()
mydog.whoAmi()
mydog.eat()
mydog.bark()

# res:
# Animal created
# Animal
# Eating
# Animal created
# Dog created
# Animal
# Dog Eating
# Woof
# [Finished in 0.1s]
