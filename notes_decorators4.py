def hello(name = "Jose"):
    return "hello "+name

print(hello())

mynewgreet = hello

print(mynewgreet())

# result:
# hello Jose
# hello Jose
# [Finished in 0.1s]
