s = "GLOBAL VARIABLE!"

def func():
    mylocal = 10
    # print(locals())
    print(globals()['s'])

func()

# result:

# GLOBAL VARIABLE!
# [Finished in 0.1s]
