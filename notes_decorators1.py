s = "GLOBAL VARIABLE!"

def func():
    mylocal = 10
    print(locals())


func()

# result: {'mylocal': 10}
