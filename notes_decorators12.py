def hello():
    return "HI JOSE!"

def other(func):
    print("HELLO")
    print(func)

other(hello)

# result:
# HELLO
# <function hello at 0x7fe99c809378>
# [Finished in 0.1s]
