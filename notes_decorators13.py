def hello():
    return "HI JOSE!"

def other(func):
    print("HELLO")
    print(func())

other(hello)

# result:
# HELLO
# HI JOSE!
# [Finished in 0.1s]
