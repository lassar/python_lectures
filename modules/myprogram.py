import mymodule
mymodule.func_in_module()

# or

import mymodule as mm
mm.func_in_module()

#or

from mymodule import func_in_module
func_in_module()

# or
from mymodule import *
func_in_module()

# or
