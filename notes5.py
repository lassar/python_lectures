class Dog():
    """docstring for Dog"""

    def __init__(self, name):
        self.breed = name

mydog = Dog(name = "Lab")
otherdog = Dog(name="Huskie")
print(mydog.breed)
print(otherdog.breed)

# res:
# Lab
# Huskie
# [Finished in 0.1s]
