s = "GLOBAL VARIABLE!"

def func():
    mylocal = 10
    print(locals())
    print(globals())


func()

# result: {'mylocal': 10}
# {'__file__': '/home/larac/sites/django_lectures/notes_decorators2.py', '__cached__': None, 'func': <function func at 0x7f91a5c24378>, 's': 'GLOBAL VARIABLE!', '__name__': '__main__', '__package__': None, '__doc__': None, '__spec__': None, '__loader__': <_frozen_importlib_external.SourceFileLoader object at 0x7f91a5cf2550>, '__builtins__': <module 'builtins' (built-in)>}
# [Finished in 0.1s]
