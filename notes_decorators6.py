def hello(name="jose"):
    print("THE HELLO() FUNCTION HAS BEEN RUN!")

    def greet():
        return "THIS STRING IS INSIDE GREET()"

    def welcome():
        return "THIS STRING IS INSIDE WELCOME"

hello()

# result:
# THE HELLO() FUNCTION HAS BEEN RUN!
# [Finished in 0.1s]
