def hello(name="jose"):
    print("THE HELLO() FUNCTION HAS BEEN RUN!")

    def greet():
        return "THIS STRING IS INSIDE GREET()"

    def welcome():
        return "THIS STRING IS INSIDE WELCOME"

    print(greet())
    print(welcome())
    print("END of hello()")

hello()

# result:
# THE HELLO() FUNCTION HAS BEEN RUN!
# THIS STRING IS INSIDE GREET()
# THIS STRING IS INSIDE WELCOME
# END of hello()
# [Finished in 0.1s]
