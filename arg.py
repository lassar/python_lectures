def test_var_args(f_arg, *argv):
    print("Первый позиционный аргумент:", f_arg)
    for arg in argv:
        print("Другой аргумент из *argv:", arg)

test_var_args('yasoob', 'python', 'eggs', 'test')

# res:
# Первый позиционный аргумент: yasoob
# Другой аргумент из *argv: python
# Другой аргумент из *argv: eggs
# Другой аргумент из *argv: test
# [Finished in 0.1s]

