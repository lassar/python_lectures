def decorator(func_to_decorate):
    def wrapper():
        print('Before function runs')
        func_to_decorate()
        print('After function runs')
    return wrapper


def print_string():
    print("I'am print_string() function")


print_string()

print("decorator________________:")

@decorator
def print_string():
    print("I'am print_string() function")

print_string()

# res:
# I'am print_string() function
# decorator________________:
# Before function runs
# I'am print_string() function
# After function runs
# [Finished in 0.0s]

