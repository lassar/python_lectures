class Dog():
    """docstring for Dog"""
    # CLASS OBJECTS ATTRIBUTE
    species = "mammal"

    def __init__(self, breed, name):
        self.breed = breed
        self.name = name

mydog = Dog(breed = "Lab",name = "Sammy")
otherdog = Dog(breed="Huskie",name = "Djack")
print(mydog.breed)
print(mydog.name)
print(otherdog.breed)
print(otherdog.name)
print(mydog.species)

# res:
# Lab
# Sammy
# Huskie
# Djack
# mammal
# [Finished in 0.1s]
