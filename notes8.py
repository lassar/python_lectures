# SPECIAL METHODS
class Book():
    """docstring for Book"""
    def __init__(self, title, author, pages):
        self.title = title
        self.author = author
        self.pages = pages

    def __str__(self):
        return "Title: {}, Author: {}, Pages: {}".format(
            self.title, self.author, self.pages)

    def __len__(self):
        return self.pages

    def __del__(self):
        print("a book is destroyed")


b = Book("Python","jose",200)
print(b)
print(len(b))
del b

mylist = [1,2,"alfa"]
print(mylist)

# res:
# Title: Python, Author: jose, Pages: 200
# 200
# a book is destroyed
# [1, 2, 'alfa']
# [Finished in 0.1s]
