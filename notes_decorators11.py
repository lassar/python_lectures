def hello(name="jose"):
    print("THE HELLO() FUNCTION HAS BEEN RUN!")

    def greet():
        return "THIS STRING IS INSIDE GREET()"

    def welcome():
        return "THIS STRING IS INSIDE WELCOME"

    if name == 'jose':
        return greet
    else:
        return welcome

x = hello()
print(x())

# result:
# THE HELLO() FUNCTION HAS BEEN RUN!
# THIS STRING IS INSIDE GREET()

# [Finished in 0.1s]
