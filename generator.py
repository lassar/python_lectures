def generator_function():
    for i in range(10):
        yield i


for key in generator_function():
    print(key)
