def new_decorator(func):

    def wrap_func():
        print("CODE HERE BEFORE EXECUTING FUNC")
        func()
        print("FUNC() HAS BEEN CALLED")

    return wrap_func



@new_decorator
def func_needs_decorator():
    print("THIS FUNCTION IS IN NEED JF DECORATOR!")

# func_needs_decorator = new_decorator(func_needs_decorator)


func_needs_decorator()

# result:
# CODE HERE BEFORE EXECUTING FUNC
# THIS FUNCTION IS IN NEED JF DECORATOR!
# FUNC() HAS BEEN CALLED
# [Finished in 0.1s]
