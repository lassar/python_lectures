# LOCALE
# lambda x: x**2
name = 'This is a global name!'

def greet():
    name = "sammy"

    def hello():
        print("hello "+name)

    hello()


greet()

# res:
# hello sammy
# [Finished in 0.1s]
